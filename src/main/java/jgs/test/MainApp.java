package jgs.test;

import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.AmazonSQSClientBuilder;
import com.amazonaws.services.sqs.model.SendMessageRequest;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.Map;

@SpringBootApplication
public class MainApp {

    public static void main(String[] args) {
  //      SpringApplication.run(MainApp.class);
        MainApp app = new MainApp();
        app.sendMessage("{\"determinationId\":100019}");
    }

    public void sendMessage(String message){
        final AmazonSQS sqs = AmazonSQSClientBuilder.defaultClient();

        SendMessageRequest send_msg_request = new SendMessageRequest()
                .withQueueUrl("https://sqs.ap-southeast-2.amazonaws.com/564656159859/TM-jennifer.fifo")
                .withMessageBody(message)
                .withMessageGroupId("G1");

        sqs.sendMessage(send_msg_request);
    }
}
